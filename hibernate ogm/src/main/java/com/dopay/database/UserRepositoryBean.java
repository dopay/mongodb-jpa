package com.dopay.database;

import com.dopay.database.model.Customer;
import com.dopay.database.model.Order;
import com.dopay.database.model.OrderLine;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;


@Stateless
@LocalBean
public class UserRepositoryBean {

    @Inject
    EntityManager em;


    public Order order() {
        // Customer into NoSQL
        Customer customer = new Customer();
        customer.setName("myfear");
        em.persist(customer);
        // Order into NoSQL
        Order order = new Order();
        order.setCustomer(customer);
        order.setDescription("Pinball maschine");

        // Order Lines mapping NoSQL --- RDBMS


        addOrderLine(order, new OrderLine(order, 2999));
        addOrderLine(order, new OrderLine(order, 59));
        addOrderLine(order, new OrderLine(order, 129));

        em.persist(order);
        return order;
    }

    private void addOrderLine(Order order, OrderLine orderLine) {
        em.persist(orderLine);
        order.addOrderLine(orderLine);
    }


    public Order findByUsername(Long id) {
        return em.find(Order.class, id);
    }

}