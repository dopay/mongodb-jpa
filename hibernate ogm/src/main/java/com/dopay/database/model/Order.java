package com.dopay.database.model;

import javax.persistence.Basic;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Order, stored as a root JSON object, nesting its order lines in the sane
 * document.
 *
 * @author James Sutherland
 */
@Entity
public class Order implements Serializable {
    /* The id uses the generated OID (UUID) from Mongo. */

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "order")
    @TableGenerator(
            name = "order",
            table = "sequences",
            pkColumnName = "key",
            pkColumnValue = "order",
            valueColumnName = "seed"
    )
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Long id;
    /* Optimistic locking is supported. */

    private long version;

    private String description;

    private double totalCost = 0;
    /* A nested embeddable value is stored as Embedded. */

    private Address billingAddress;

    private Address shippingAddress;


    private List<OrderLine> orderLines = new ArrayList<OrderLine>();
    /* Relationships are supported, the id is stored as a foreign key, for OneToMany a collection of ids would be stored. */

    private Customer customer;

    public Order() {
    }

    @Version
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Basic
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    @OneToMany(targetEntity = OrderLine.class, mappedBy = "order", fetch = FetchType.EAGER)
    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Embedded
    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    @Embedded
    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * Add the order line to the order, and set the back reference and update
     * the order cost.
     */
    public void addOrderLine(OrderLine orderLine) {
        getOrderLines().add(orderLine);
        orderLine.setLineNumber(getOrderLines().size());
        setTotalCost(getTotalCost() + orderLine.getCost());
    }

    public String toString() {
        return "Order(" + description + ", " + totalCost + ")";
    }
}