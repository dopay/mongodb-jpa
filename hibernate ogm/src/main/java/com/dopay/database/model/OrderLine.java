package com.dopay.database.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * OrderLine, stored as part of the Order document.
 *
 * @author James Sutherland
 */
@Entity
public class OrderLine implements Serializable {


    private String id;


    private Order order;


    private int lineNumber;


    private double cost = 0;

    public OrderLine() {
    }

    public OrderLine(Order order, double cost) {
        this.order = order;
        this.cost = cost;
    }

    @Basic
    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    @Basic
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @ManyToOne
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}