package com.dopay.database.model;

import org.eclipse.persistence.nosql.annotations.DataFormatType;
import org.eclipse.persistence.nosql.annotations.NoSql;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * OrderLine, stored as part of the Order document.
 *
 * @author James Sutherland
 */
@Embeddable
@NoSql(dataFormat = DataFormatType.MAPPED)
public class OrderLine implements Serializable {

    @Basic
    private int lineNumber;

    @Basic
    private double cost = 0;

    public OrderLine() {
    }

    public OrderLine(double cost) {

        this.cost = cost;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }


    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}