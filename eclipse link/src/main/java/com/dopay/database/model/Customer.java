package com.dopay.database.model;

import org.eclipse.persistence.nosql.annotations.DataFormatType;
import org.eclipse.persistence.nosql.annotations.Field;
import org.eclipse.persistence.nosql.annotations.NoSql;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Customer, stored as a root JSON object.
 *
 * @author James Sutherland
 */
@Entity
@NoSql(dataFormat = DataFormatType.MAPPED)
public class Customer implements Serializable {
    /* The id uses the generated OID (UUID) from Mongo. */

    @Id
    @GeneratedValue
    @Field(name = "_id")
    private String id;
    @Basic
    private String name;

    public Customer() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}